// Array re-assignmet
let a, b, c, d

[a, b, c, d] = [3,6,9,12]

console.log(a)
console.log(b)
console.log(c)
console.log(d)

// Object re-assignment (ES7)
let e, f, g, h

const strObj = ({ e, f, g, h } = { e: 'Hello', f: 'There', g: 'Matey', h: 'Mate', j: 'Not!'})

console.log(strObj)

console.log(e)
console.log(f)
console.log(g)
console.log(h)

// Object rest example

const buildString = function({e, f, g, h, k = ':D'}) {
  console.log(arguments)
  return [e, f, g, h, k].join(' ')
}

console.log(buildString(strObj))

// Rest

const arrayBegin = ['god','save']

const arrayMiddle = 'the'

const arrayEnd = ['queen']

const arrayMore = ['our','gracious']

console.log(arrayBegin)

console.log([...arrayBegin, ...arrayMiddle, ...arrayEnd])

console.log([...arrayBegin, ...arrayMore, ...arrayEnd])

console.log(...[...arrayBegin, ...arrayMore, ...arrayEnd])

// Rest example

const singForMe = function() {
  return [...arguments].join(' ')
}

const lyrics = ['Mary', 'had', 'a', 'little', 'lamb']

console.log(singForMe(lyrics))

console.log(singForMe(...lyrics))

console.log(singForMe('Up', 'town', 'funk'))
