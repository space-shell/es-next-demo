// Simple...
const hello = `Helloooo!`

const world = `earth`

const eggo = '\n' + hello + '\n\'awesome\'\n' + world + ' ' + (2 + 2) + '.0'

const elo = `
  ${hello}
  'awesome'
  ${world} ${2 + 2}.0
`

console.log(eggo)
console.log(elo)
