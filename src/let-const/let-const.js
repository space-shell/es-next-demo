console.log(first)
console.log(second)
console.log(third)

var string = ''

let first = 'Hello'

var second = 'there'

const third = 'matey'; // One of the very few cases for semi-colons

//third = 'buddy' // Cannot reassign a constant

(function update() {
  let fourth = 'Goodbye'
  var fifth = 'old';

  (function updated() {
    //fourth = 'blah' //Re assign a 'let varable out of scope'
    fifth = 'blob'
  })()

  first = fourth
  second = fifth;

  string = `${first} ${second} ${third}`
})()

console.log(first)
console.log(second)
console.log(third)

console.log(string)
