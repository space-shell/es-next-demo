import fetch from 'node-fetch'

// Fetches a number of random quotes
export default class randomQuotes {
  constructor(
    private url:string,
    private ammount:number = 1
  ){}

  private async quotesGet() {
    return await fetch(this.url)
  }

  get info(): object {
    return this.quotesGet().then(j => j.json()).then(q => console.log(q))
  }
}

let quote: string = 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1'

const q = new randomQuotes(quote)

console.log(q.info)
