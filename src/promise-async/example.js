import fetch from 'node-fetch'

// Promise example

let quoteUri = 'http://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=1'
//quoteUri = 'https://blank.blank'

const myQuote = fetch(quoteUri)

const getQuotes = () => {
  console.log('Starting quote download')
  return myQuote
    .then(response => response.json())
    .then(json => console.log(json))
    .then(quote => console.log(quote.contents.quotes))
    .catch(err => console.log(err))
}

console.log(getQuotes())

// babel-node src/promise-async/example.js
