const arrow = input => {
  console.log(input)
  console.log(this)
  return input
}

const arrowSmall = value => value

function hoisted(words) {
  console.log(this)
  return words
}

const assigned = function(numbers) {
  console.log(this)
  return numbers
}

const that = {
  variable: 'Where am I',
  arrow: () => {
    console.log(this)
    return this
  },
  func: function() {
    console.log(this)
    return this
  }
}

console.log(arrow('Hello'))

console.log(arrowSmall('World'))

console.log(hoisted('Top top top'))

console.log(assigned(111))

console.log(that)

console.log(that.arrow())

console.log(that.func())
