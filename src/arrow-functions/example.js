const arrow = () => {
  return this.variable
}

const func = function() {
  return this.variable
}

const scope = type => {
  this.variable = 'Hello Arrow'
  return {
    variable: 'Hello Function',
    arrow,
    func
  }
}

console.log(scope().arrow())

console.log(scope().func())

//node src/arrow-functions/example.js
